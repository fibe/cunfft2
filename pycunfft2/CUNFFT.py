import ctypes
import os
import numpy.ctypeslib as ctl
import numpy as np


class CUNFFT:

    def __init__(self):
        libname = 'libcunfft.so';
        libdir = os.path.dirname(__file__)+'/../lib/';
        print("Loading lib "+libdir+libname);
        self.lib = ctypes.CDLL( libdir + libname );

        # Init function definitions
        self.getGPUMemProps = self.lib.getGPUMemProps_ToStdout;

        self.resetDevice = self.lib.resetDevice;

        self.mallocPlan = self.lib.mallocPlan;
        self.freePlan = self.lib.freePlan;
        self.freePlan.argtypes = [ctypes.c_voidp ];

        self.cunfft_init = self.lib.cunfft_init;
        self.cunfft_init.argtypes = [ctypes.c_voidp, ctypes.c_uint32, ctl.ndpointer(np.uint32, flags='aligned, c_contiguous'), ctypes.c_uint32 ];

        self.copyDataToDevice = self.lib.copyDataToDevice;
        self.copyDataToDevice.argtypes = [ ctypes.c_voidp ];

        self.copyDataToHost = self.lib.copyDataToHost;
        self.copyDataToHost.argtypes = [ ctypes.c_voidp ];

        self.getExampleData_uniDistr = self.lib.getExampleData_uniDistr
        self.getExampleData_uniDistr.argtypes = [ ctypes.c_voidp ];
        self.debugPlanData = self.lib.debugPlanData
        self.debugPlanData.argtypes = [ ctypes.c_voidp ];


        self.finalize = self.lib.cunfft_finalize

        self.set_x = self.lib.set_x;
        self.set_x.argtypes = [ ctypes.c_voidp, ctl.ndpointer( np.float32, flags='aligned, c_contiguous')];

        self.set_fhat = self.lib.set_fhat;
        self.set_fhat.argtypes = [ ctypes.c_voidp, ctl.ndpointer( np.complex64, flags='aligned, c_contiguous')];

        self.set_f = self.lib.set_f;
        self.set_f.argtypes = [ ctypes.c_voidp, ctl.ndpointer( np.complex64, flags='aligned, c_contiguous')];


        self.fft_adjoint = self.lib.cunfft_adjoint;
        self.fft_adjoint.argtypes = [ ctypes.c_voidp ];

        self.fft_transform = self.lib.cunfft_transform;
        self.fft_transform.argtypes = [ ctypes.c_voidp ];

        self.copy_f_ToHost = self.lib.copy_f_ToHost;
        self.copy_f_ToHost.argtypes = [ ctypes.c_voidp, ctl.ndpointer( np.complex64, flags='aligned, c_contiguous')];

        self.copy_f_hat_ToHost = self.lib.copy_f_hat_ToHost;
        self.copy_f_hat_ToHost.argtypes = [ ctypes.c_voidp, ctl.ndpointer( np.complex64, flags='aligned, c_contiguous')];


if __name__ == "__main__":
    print("CUNFFT test");
    cunfft = CUNFFT();

    plan = cunfft.mallocPlan();

    cunfft.cunfft_init( plan, 2, np.array( [32,32], dtype=np.uint32 ), 128 );
    cunfft.getExampleData_uniDistr( plan );
    cunfft.debugPlanData( plan );

    cunfft.copyDataToDevice( plan );
    cunfft.fft_transform( plan );

    f=np.zeros( (128,1), dtype=np.complex64 );
    cunfft.copy_f_ToHost( plan, f );

    cunfft.copyDataToHost( plan );

    print(f);
    cunfft.debugPlanData( plan );
    cunfft.finalize( plan );
    cunfft.freePlan( plan );
